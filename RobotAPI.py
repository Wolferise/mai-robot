import pickle


class ChessRobotAPI:
    def __init__(self):
        self.trajectory = Trajectory()
        self.chess_board = ChessBoard()
        self.start_pos = ''
        self.end_pos = ''

    def set_chess_move(self):
        with open('/Users/ilya/Desktop/Robotic-Chess/data.txt', 'r') as f:
            moves = f.read().split('\n')
            self.start_pos, self.end_pos = moves[-1].split()
            f.close()

    def calculate_robot_movement(self):
        self.set_chess_move()
        start_pos_top = self.start_pos + "-top"
        start_pos_bottom = self.start_pos + "-bottom"
        end_pos_top = self.end_pos + "-top"
        end_pos_bottom = self.end_pos + "-bottom"
        if self.chess_board.piece_is_in_position(self.end_pos):
            self.trajectory.instruction_set.append('##GETTING PIECE AWAY##')
            self.trajectory.add_new_point(end_pos_top)
            self.trajectory.add_new_point(end_pos_bottom)
            self.trajectory.add_new_point(end_pos_top)
            self.trajectory.add_new_point('ejection-pos')
        self.trajectory.instruction_set.append('##MOVING PIECE##')
        self.trajectory.add_new_point(start_pos_top)
        self.trajectory.add_new_point(start_pos_bottom)
        self.trajectory.add_new_point(start_pos_top)
        self.trajectory.add_new_point(end_pos_top)
        self.trajectory.add_new_point(end_pos_bottom)
        self.trajectory.add_new_point(end_pos_top)
        self.trajectory.add_new_point('observer-pos')
        self.trajectory.set_instruction_set()
        self.trajectory.clear()
        self.trajectory.current_instruction_line.clear_instruction_line()
        self.start_pos = ''
        self.end_pos = ''


class Trajectory:
    def __init__(self):
        self.instruction_set = []
        self.current_instruction_line = Instruction()

    def add_new_point(self, position):
        self.current_instruction_line.set_instruction_line(position)
        self.instruction_set.append(self.current_instruction_line.instruction_line)

    def clear(self):
        self.instruction_set = []

    def set_instruction_set(self):
        instruction_file = open('/Users/ilya/Downloads/ARCS source files/fuck', 'wb')
        pickle.dump(self.instruction_set, instruction_file)
        instruction_file.close()


class ChessBoard:
    def __init__(self):
        self.cells = [[False for i in range(8)] for j in range(8)]

    def set_piece_positions(self, positions):
        for i, j in self.cells:
            self.cells[i][j] = positions[i][j]

    def piece_is_in_position(self, position):
        check = self.cells[position_translate_dict.get(position[0]) - 1][int(position[1]) - 1]
        return check


class Instruction:
    def __init__(self):
        self.instruction_line = ""

    def set_instruction_line(self, position):
        self.instruction_line = ""
        self.instruction_line += "Move L [*]  X) "
        self.instruction_line += str(position_dict.get(position)[0])
        self.instruction_line += "   Y) "
        self.instruction_line += str(position_dict.get(position)[1])
        self.instruction_line += "   Z) "
        self.instruction_line += str(position_dict.get(position)[2])
        self.instruction_line += "   W) "
        self.instruction_line += str(position_dict.get(position)[3])
        self.instruction_line += "   P) "
        self.instruction_line += str(position_dict.get(position)[4])
        self.instruction_line += "   R) "
        self.instruction_line += str(position_dict.get(position)[5])
        self.instruction_line += "   T) 201.5   Speed-25 Ad 15 As 10 Dd 20 Ds 5 $F"
        self.instruction_line += '\n'

    def clear_instruction_line(self):
        self.instruction_line = ""


position_translate_dict = {'a': 1,
                           'b': 2,
                           'c': 3,
                           'd': 4,
                           'e': 5,
                           'g': 6,
                           'h': 7,
                           'i': 8}
position_dict = {'a1-top': ['a1-top', 'a1-top', 'a1-top', 'a1-top', 'a1-top', 'a1-top'],
                 'a1-bottom': [],
                 'a2-top': ['a2-top', 'a2-top', 'a2-top', 'a2-top', 'a2-top', 'a2-top'],
                 'a2-bottom': ['a2-bottom', 'a2-bottom', 'a2-bottom', 'a2-bottom', 'a2-bottom', 'a2-bottom'],
                 'a3-top': [],
                 'a3-bottom': [],
                 'a4-top': ['a4-top', 'a4-top', 'a4-top', 'a4-top', 'a4-top', 'a4-top'],
                 'a4-bottom': ['a4-bottom', 'a4-bottom', 'a4-bottom', 'a4-bottom', 'a4-bottom', 'a4-bottom'],
                 'a5-top': [],
                 'a5-bottom': [],
                 'a6-top': [],
                 'a6-bottom': [],
                 'a7-top': [],
                 'a7-bottom': [],
                 'a8-top': [],
                 'a8-bottom': [],
                 'b1-top': [],
                 'b1-bottom': [],
                 'b2-top': [],
                 'b2-bottom': [],
                 'b3-top': ['b3-top', 'b3-top', 'b3-top', 'b3-top', 'b3-top', 'b3-top'],
                 'b3-bottom': ['b3-bottom', 'b3-bottom', 'b3-bottom', 'b3-bottom', 'b3-bottom', 'b3-bottom'],
                 'c1-top': ['c1-top', 'c1-top', 'c1-top', 'c1-top', 'c1-top', 'c1-top'],
                 'c1-bottom': ['c1-bottom', 'c1-bottom', 'c1-bottom', 'c1-bottom', 'c1-bottom', 'c1-bottom'],
                 'e2-top': ['e2-top', 'e2-top', 'e2-top', 'e2-top', 'e2-top', 'e2-top'],
                 'e2-bottom': ['e2-bottom', 'e2-bottom', 'e2-bottom', 'e2-bottom', 'e2-bottom', 'e2-bottom'],
                 'e3-top': [],
                 'e3-bottom': [],
                 'e4-top': ['e4-top', 'e4-top', 'e4-top', 'e4-top', 'e4-top', 'e4-top'],
                 'e4-bottom': ['e4-bottom', 'e4-bottom', 'e4-bottom', 'e4-bottom', 'e4-bottom', 'e4-bottom'],
                 'observer-pos': ['observer-pos', 'observer-pos', 'observer-pos', 'observer-pos', 'observer-pos', 'observer-pos'],
                 'ejection-pos': ['ejection-pos', 'ejection-pos', 'ejection-pos', 'ejection-pos', 'ejection-pos', 'ejection-pos']}

chessRobotApi = ChessRobotAPI()
chessRobotApi.calculate_robot_movement()
